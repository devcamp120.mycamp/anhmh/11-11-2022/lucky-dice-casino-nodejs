//Import bộ thư viện Express
const express = require('express');

//Khai báo app Router
const prizeHistoryRouter = express.Router();

//Import controller
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById } = require("../controllers/prizeHistoryController");

//Khai báo API 
prizeHistoryRouter.post("/prize-histories", createPrizeHistory);

prizeHistoryRouter.get("/prize-histories", getAllPrizeHistory);

prizeHistoryRouter.get("/prize-histories/:historyId", getPrizeHistoryById);

prizeHistoryRouter.put("/prize-histories/:historyId", updatePrizeHistoryById);

prizeHistoryRouter.delete("/prize-histories/:historyId", deletePrizeHistoryById);

module.exports = prizeHistoryRouter;