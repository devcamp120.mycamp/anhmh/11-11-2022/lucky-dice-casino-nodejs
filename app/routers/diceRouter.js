//Import bộ thư viện express
const express = require('express');

//Import module controllers
const { diceHandler, getDiceHistoryByUsername, getPrizeHistoryByUsername, getVoucherHistoryByUsername } = require("../controllers/diceController");
const diceRouter = express.Router();

//Create a dice
diceRouter.post("/devcamp-lucky-dice/dice", diceHandler);

//Get a dice history by username
diceRouter.get("/devcamp-lucky-dice/dice-history?username=:username", getDiceHistoryByUsername);

//Get a prize history by username
diceRouter.get("/devcamp-lucky-dice/prize-history?username=:username", getPrizeHistoryByUsername);

//Get a voucher history by username
diceRouter.get("/devcamp-lucky-dice/voucher-history?username=:username", getVoucherHistoryByUsername);

//Export dữ liệu thành 1 module
module.exports = diceRouter;