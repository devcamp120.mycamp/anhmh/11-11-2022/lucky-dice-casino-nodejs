//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import module controllers
const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require("../controllers/voucherController");
const voucherRouter = express.Router();

//Create a voucher
voucherRouter.post("/vouchers", createVoucher);

//Get all voucher
voucherRouter.get("/vouchers", getAllVoucher);

//Get voucher by id
voucherRouter.get("/vouchers/:voucherId", getVoucherById);

//Update voucher by id
voucherRouter.put("/vouchers/:voucherId", updateVoucherById);

//Delete voucher by id
voucherRouter.delete("/vouchers/:voucherId", deleteVoucherById);

//Export dữ liệu thành 1 module
module.exports = voucherRouter;