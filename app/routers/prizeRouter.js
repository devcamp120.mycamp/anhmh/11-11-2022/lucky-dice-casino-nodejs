//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import module controllers
const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deleteUserById } = require("../controllers/prizeController");
const prizeRouter = express.Router();

//Create a prize
prizeRouter.post("/prizes", createPrize);

//Get all prize
prizeRouter.get("/prizes", getAllPrize);

//Get prize by id
prizeRouter.get("/prizes/:prizeId", getPrizeById);

//Update prize by id
prizeRouter.put("/prizes/:prizeId", updatePrizeById);

//Delete prize by id
prizeRouter.delete("/prizes/:prizeId", deleteUserById);

//Export dữ liệu thành 1 module
module.exports = prizeRouter;