//Import bộ thư viện Express
const express = require('express');

//Khai báo app Router
const voucherHistoryRouter = express.Router();

//Import controller
const { createVoucherHistory, getAllVoucherHistory, getHistoryVoucherById, updateVoucherHistoryById, deleteVoucherHistoryById } = require("../controllers/voucherHistoryController");

voucherHistoryRouter.post("/voucher-histories", createVoucherHistory);

voucherHistoryRouter.get("/voucher-histories", getAllVoucherHistory);

voucherHistoryRouter.get("/voucher-histories/:historyId", getHistoryVoucherById);

voucherHistoryRouter.put("/voucher-histories/:historyId", updateVoucherHistoryById);

voucherHistoryRouter.delete("/voucher-histories/:historyId", deleteVoucherHistoryById);

module.exports = voucherHistoryRouter;