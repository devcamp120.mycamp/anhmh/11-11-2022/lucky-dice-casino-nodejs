//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import module controllers
const { createUser, getAllUser, getAllUserById, updateUserById, deleteUserById } = require("../controllers/userController");
const userRouter = express.Router();

//Create a user
userRouter.post("/users", createUser);

//Get all user
userRouter.get("/users", getAllUser);

//Get a user by id
userRouter.get("/users/:userId", getAllUserById);

//Update a user by id
userRouter.put("/users/:userId", updateUserById);

//Delete a user by id
userRouter.delete("/users/:userId", deleteUserById);

//Export dữ liệu thành 1 module
module.exports = userRouter;