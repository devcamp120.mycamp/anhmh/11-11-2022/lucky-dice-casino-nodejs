//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createDiceHistory, getAllDiceHistory, getAllDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById } = require("../controllers/diceHistoryController");
const diceHistoryRouter = express.Router();

//Create a user
diceHistoryRouter.post("/dice-histories", createDiceHistory);

//Get all user
diceHistoryRouter.get("/dice-histories", getAllDiceHistory);

//Get a user by id
diceHistoryRouter.get("/dice-histories/:diceHistoryId", getAllDiceHistoryById);

//Update a user by id
diceHistoryRouter.put("/dice-histories/:diceHistoryId", updateDiceHistoryById);

//Delete a user by id
diceHistoryRouter.delete("/dice-histories/:diceHistoryId", deleteDiceHistoryById);

//Export dữ liệu thành 1 module
module.exports = diceHistoryRouter;