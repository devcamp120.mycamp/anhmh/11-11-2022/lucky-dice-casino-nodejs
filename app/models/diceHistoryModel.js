//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const diceHistorySchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
    },
    dice: {
        type: Number,
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Dice History", diceHistorySchema);

/*
DiceHistory: {
    _id: ObjectId, unique
    user: ObjectID, ref: User, required
    dice: Number, required
createdAt: Date, default: Date.now()
updatedAt: Date, default: Date.now()
}
*/