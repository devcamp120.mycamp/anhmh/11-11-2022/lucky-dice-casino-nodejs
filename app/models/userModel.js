//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const userSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    username: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("User", userSchema);


/*
User: {
	_id: ObjectId, unique
	username: String, unique, required
	firstname: String, required
	lastname: String, required
	createdAt: Date, default: Date.now()
updatedAt: Date, default: Date.now()
}
*/