//Bước 1: Khai báo thư viện moongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const voucherSchema = new Schema({
	__id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
	code: {
		type: String,
        unique: true,
        required: true
	},
	discount: {
		type: Number,
		required: true
	},
	note: {
		type: String,
		required: false
	},
	createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Voucher", voucherSchema);
/*
Voucher: {
	_id: ObjectId, unique
	code: String, unique, required
	discount: Number, required
	note: String, not required
createdAt: Date, default: Date.now()
updatedAt: Date, default: Date.now()
}

*/