//Bước 1: Khai báo thư viện moongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const prizeSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        unique: true,
        required: true

    },
    description: {
        type: String,
        required: false
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Prize", prizeSchema);

/*
Prize: {
	_id: ObjectId, unique
	name: String, unique, required
	description: String, not required
	createdAt: Date, default: Date.now()
updatedAt: Date, default: Date.now()
}
*/