//Bước 1: Khai báo thư viện Moongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const voucherHistorySchema = new Schema({
	__id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
	user: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
    },
	voucher: {
            type: mongoose.Types.ObjectId,
            ref: "Voucher",
            required: true
    },
	createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})
//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Voucher History", voucherHistorySchema);
/*
VoucherHistory: {
	_id: ObjectId, unique
	user: ObjectID, ref: User, required
	voucher: ObjectID, ref: Voucher, required
    createdAt: Date, default: Date.now()
    updatedAt: Date, default: Date.now()
}
*/