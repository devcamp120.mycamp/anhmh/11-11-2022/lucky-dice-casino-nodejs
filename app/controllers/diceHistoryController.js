const mongoose = require("mongoose");

const userModel = require("../models/userModel");
const diceHistoryModel = require("../models/diceHistoryModel");
const { response, request } = require("express");

//Create a dice history
const createDiceHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!Number.isInteger(requestBody.dice) || requestBody.dice < 0 || requestBody.dice > 6) {
        return response.status(400).json({
            message: "[dice] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        let newDiceHistoryInput = {
            _id: mongoose.Types.ObjectId(),
            user: mongoose.Types.ObjectId(),
            dice: requestBody.dice
        }
        console.log(newDiceHistoryInput)
        diceHistoryModel.create(newDiceHistoryInput, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(201).json({
                    status: "Create Dice History Success",
                    data: data
                })
            }
        })
    }
}
//Get all dice history
const getAllDiceHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let user = request.query.user;
    if(user){
        condition.user = user;
    }
    console.log(condition);
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    diceHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Dice History",
                data: data
            })
        }
    })
}

//Get dice history by id
const getAllDiceHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        response.status(400).json({
            message: "[diceHistoryId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        diceHistoryModel.findById(diceHistoryId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Dice By Id",
                    data: data
                })
            }
        })
    }
}

//Update dice history by id
const updateDiceHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        response.status(400).json({
            message: "[diceHistoryId] is invalid!"
        })
    }
    if (!Number.isInteger(requestBody.dice) || requestBody.dice < 0 || requestBody.dice > 6) {
        return response.status(400).json({
            message: "[dice] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        let updateDiceHistoryById = {
            dice: requestBody.dice
        }
        console.log(updateDiceHistoryById)
        diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistoryById, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Update Dice History By Id",
                    data: data
                })
            }
        })
    }
}

//Delete dice history by id
const deleteDiceHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        response.status(400).json({
            message: "[diceHistoryId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Dice History Success",
                    data: data
                })
            }
        })
    }
}

module.exports = { createDiceHistory, getAllDiceHistory, getAllDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById }