const mongoose = require("mongoose");

const prizeHistoryModel = require("../models/prizeHistoryModel");
const { response, request } = require("express");

//Create a voucher history
const createPrizeHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;
    console.log(requestBody)
    //{ userId: '637301aae6ef84ba7cda68f6', prizeId: 'djfajfladaklsdfl' }
    let userId = requestBody.userId;
    let prizeId = requestBody.prizeId;
    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.userId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is required!"
        })
    }
    if (!requestBody.prizeId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[prizeId] is required!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    let newPrizeHistoryInput = {
        _id: mongoose.Types.ObjectId(),
        user: requestBody.userId,
        prize: requestBody.prizeId
    }
    prizeHistoryModel.create(newPrizeHistoryInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Create Prize History Success",
                data: data
            })
        }
    })
}


//Get all prize history
const getAllPrizeHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện tao tác dữ liệu
    prizeHistoryModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Get All Prize History Success",
                data: data
            })
        }
    })
}

//Get a prize history by id
const getPrizeHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            message: "[historyId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu 
        prizeHistoryModel.findById(historyId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get A Prize History By Id",
                    data: data
                })
            }
        })
    }
}

//Update a prize history by id
const updatePrizeHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;
    let requestBody = request.body;
    let userId = requestBody.userId;
    let prizeId = requestBody.prizeId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[historyId] is invalid!"
        })
    }
    if (!requestBody.user) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is required!"
        })
    }
    if (!requestBody.prize) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[prizeId] is required!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updatePrizeHistoryInput = {
            user: requestBody.userId,
            prize: requestBody.prizeId
        }
        console.log(updatePrizeHistoryInput)
        prizeHistoryModel.findByIdAndUpdate(historyId, updatePrizeHistoryInput, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Bad request",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Update Prize History By Id",
                    data: data
                })
            }
        })
    }
}

//Delete a prize history by id
const deletePrizeHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            message: "[historyId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        prizeHistoryModel.findByIdAndDelete(historyId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Prize History Success",
                    data: data
                })
            }
        })
    }
}
module.exports = { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById }