const mongoose = require("mongoose");

const voucherHistoryModel = require("../models/voucherHistoryModel");
const { response, request } = require("express");

//Create a voucher history
const createVoucherHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;
    let userId = requestBody.userId;
    let voucherId = requestBody.voucherId;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.userId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is required!"
        })
    }
    if (!requestBody.voucherId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[voucherId] is required!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    let newVoucherHistoryInput = {
        _id: mongoose.Types.ObjectId(),
        user: requestBody.userId,
        voucher: requestBody.voucherId
    }
    voucherHistoryModel.create(newVoucherHistoryInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Create Voucher History Success",
                data: data
            })
        }
    })
}

//Get all voucher history
const getAllVoucherHistory = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện tao tác dữ liệu
    voucherHistoryModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Get All Voucher History Success",
                data: data
            })
        }
    })
}

//Get a voucher by id
const getHistoryVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            message: "[historyId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu 
        voucherHistoryModel.findById(historyId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get A Voucher History By Id",
                    data: data
                })
            }
        })
    }
}

//Update a voucher history by id
const updateVoucherHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;
    let requestBody = request.body;
    let userId = requestBody.userId;
    let voucherId = requestBody.voucherId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[historyId] is invalid!"
        })
    }
    if (!requestBody.userId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is required!"
        })
    }
    if (!requestBody.voucherId) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[voucherId] is required!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updateVoucherHistoryInput = {
            user: requestBody.userId,
            voucher: requestBody.prizeId
        }
        console.log(updateVoucherHistoryInput)
        voucherHistoryModel.findByIdAndUpdate(historyId, updateVoucherHistoryInput, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Bad request",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Update Voucher History By Id",
                    data: data
                })
            }
        })
    }
}

//Delete a voucher history by id
const deleteVoucherHistoryById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let historyId = request.params.historyId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            message: "[historyId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        voucherHistoryModel.findByIdAndDelete(historyId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Voucher History Success",
                    data: data
                })
            }
        })
    }
}

module.exports = { createVoucherHistory, getAllVoucherHistory, getHistoryVoucherById, updateVoucherHistoryById, deleteVoucherHistoryById }