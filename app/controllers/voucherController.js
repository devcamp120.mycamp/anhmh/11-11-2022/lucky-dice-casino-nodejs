const mongoose = require("mongoose");

const voucherModel = require("../models/voucherModel");
const { response, request } = require("express");

//Create a voucher
const createVoucher = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.code) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[code] is required!"
        })
    }
    if (!Number.isInteger(requestBody.discount) && requestBody.discount > 0) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[discount] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác nghiệp vụ
    let newVoucherInput = {
        _id: mongoose.Types.ObjectId(),
        code: requestBody.code,
        discount: requestBody.discount
    }
    console.log(newVoucherInput)
    voucherModel.create(newVoucherInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Voucher Success",
                data: data
            })
        }
    })
}

//Get all voucher
const getAllVoucher = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Get All Voucher Success",
                data: data
            })
        }
    })
}

//Get a voucher by id
const getVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let voucherId = request.params.voucherId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        response.status(400).json({
            message: "[voucherId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        voucherModel.findById(voucherId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Get A Voucher By Id",
                    data: data
                })
            }
        })
    }
}

//Update a voucher by id
const updateVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let voucherId = request.params.voucherId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            message: "[voucherId] is invalid!"
        })
    }
    if (!requestBody.code) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[code] is required!"
        })
    }
    if (!Number.isInteger(requestBody.discount) && requestBody.discount > 0) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[discount] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updateVoucherInput = {
            code: requestBody.code,
            discount: requestBody.discount
        }
        console.log(updateVoucherInput)
        voucherModel.findByIdAndUpdate(voucherId, updateVoucherInput, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Update A Voucher By Id",
                    data: data
                })
            }
        })
    }
}

//Delete a user by id
const deleteVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let voucherId = request.params.voucherId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            message: "[voucherId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        voucherModel.findByIdAndDelete(voucherId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Voucher Success",
                    data: data
                })
            }
        })
    }
}
module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById }