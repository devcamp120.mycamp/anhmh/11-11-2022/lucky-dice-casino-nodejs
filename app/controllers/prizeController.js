const mongoose = require("mongoose");

const prizeModel = require("../models/prizeModel");
const { response, request } = require("express");

//Create a prize
const createPrize = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if(!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    }
    //Bước 3: Thực hiện thao tác nghiệp vụ
    let newPrizeInput = {
        _id: mongoose.Types.ObjectId(),
        name: requestBody.name,
        description: requestBody.description
    }
    console.log(newPrizeInput)
    prizeModel.create(newPrizeInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Prize Success",
                data: data
            })
        }
    })
}

//Get all prize
const getAllPrize = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Get All Prize Success",
                data: data
            })
        }
    })
}

//Get a prize by id
const getPrizeById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let prizeId = request.params.prizeId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        response.status(400).json({
            message: "[prizeId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        prizeModel.findById(prizeId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Get A Prize By Id",
                    data: data
                })
            }
        })
    }
}

//Update a prize by id
const updatePrizeById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let prizeId = request.params.prizeId;
    let requestBody = request.body;
    
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            message: "[prizeId] is invalid!"
        })
    }
    if(!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updatePrizeInput = {
            name: requestBody.name
        }
        console.log(updatePrizeInput)
        prizeModel.findByIdAndUpdate(prizeId, updatePrizeInput, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Update A Prize By Id",
                    data: data
                })
            }
        })
    }
}

//Delete a user by id
const deleteUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let prizeId = request.params.prizeId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            message: "[prizeId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        prizeModel.findByIdAndDelete(prizeId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete User Success",
                    data: data
                })
            }
        })
    }
}

module.exports = { createPrize, getAllPrize, getPrizeById, updatePrizeById, deleteUserById }