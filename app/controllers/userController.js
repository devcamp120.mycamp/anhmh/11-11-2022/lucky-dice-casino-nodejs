const mongoose = require("mongoose");

const userModel = require("../models/userModel");
const { response, request } = require("express");

//Create a user
const createUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[username] is required!"
        })
    }
    if (!requestBody.firstname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[firstname] is required!"
        })
    }
    if (!requestBody.lastname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[lastname] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newUserInput = {
        _id: mongoose.Types.ObjectId(),
        username: requestBody.username,
        firstname: requestBody.firstname,
        lastname: requestBody.lastname
    }
    console.log(newUserInput)
    userModel.create(newUserInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create User Success",
                data: data
            })
        }
    })
}

//Get all user
const getAllUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Get All User Success",
                data: data
            })
        }
    })
}

//Get a user by id
const getAllUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        userModel.findById(userId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Get A User By Id",
                    data: data
                })
            }
        })
    }
}

//Update a user by id
const updateUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            message: "[userId] is invalid!"
        })
    }
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[username] is required!"
        })
    }
    if (!requestBody.firstname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[firstname] is required!"
        })
    }
    if (!requestBody.lastname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[lastname] is required!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let updateUserInput = {
            username: requestBody.username,
            firstname: requestBody.firstname,
            lastname: requestBody.lastname
        }
        console.log(updateUserInput)
        userModel.findByIdAndUpdate(userId, updateUserInput, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(200).json({
                    status: "Success: Update A User By Id",
                    data: data
                })
            }
        })
    }
}

//Delete a user by id
const deleteUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        userModel.findByIdAndDelete(userId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete User Success",
                    data: data
                })
            }
        })
    }
}

module.exports = { createUser, getAllUser, getAllUserById, updateUserById, deleteUserById }