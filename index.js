//Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const { response } = require("express");
const express = require("express");
const { request } = require("http");

//Khai báo thư viện path
const path = require("path");

//Khởi tạo app express 
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo thư viện Mongoose
const mongoose = require('mongoose');

//Kết nối đến cơ sở dữ liệu Mongoose
mongoose.connect('mongodb://localhost:27017/CRUD_LuckyDiceCasino', (error) => {
    if (error) {
        throw error;
    }
    console.log("Successfully connected!");
});

//Sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//Khai báo các Router (Import Router)
const userRouter = require("./app/routers/userRouter");
const diceHistoryRouter = require("./app/routers/diceHistoryRouter");
const prizeRouter = require("./app/routers/prizeRouter");
const voucherRouter = require("./app/routers/voucherRouter");
const prizeHistoryRouter = require("./app/routers/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routers/voucherHistoryRouter");
const diceRouter = require("./app/routers/diceRouter");

//Khai báo các model (Import Model)
const userModel = require("./app/models/userModel");
const diceHistoryModel = require("./app/models/diceHistoryModel");
const prizeModel = require("./app/models/prizeModel");
const voucherModel = require("./app/models/voucherModel");
const prizeHistoryModel = require("./app/models/prizeHistoryModel");
const voucherHistoryModel = require("./app/models/voucherHistoryModel"); 


//Khai báo sử dụng được body json
app.use(express.json());

//Khai báo sử dụng các tài nguyên static (images, js, css, v...v...)
app.use(express.static("views"));

//Viết middelware console ra thời gian hiện tại mỗi lần chạy
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})


// Router Front-end
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"));
})

//Sử dụng Router
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);
app.use("/", diceRouter);

//Khao báo API dạng GET "/" sẽ chạy vào đây
//Callback function: là một tham số của hàm khác và sẽ được thực thi ngay sau khi hàm đấy được gọi
/*
app.get("/random-number", (request, response) => {
    //Returns a random integer from 1 to 6
   let randomNumber = Math.floor(Math.random()* 6) + 1;

    response.status(200).json({
        message: `Số tự nhiên ngẫu nhiên bất kì từ 1 đến 6 là : ${randomNumber} `
    })
})
*/
/*
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/pizza365index.html'));
})
*/
/*
app.get("/lucky-dice", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/Task 23B.60.html'));
})
*/

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port(Ứng dụng đang chạy trên cổng) " + port);
})